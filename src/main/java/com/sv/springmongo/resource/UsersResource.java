package com.sv.springmongo.resource;


import com.sv.springmongo.document.Users;
import com.sv.springmongo.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/iuweqgfoiewnfjkdsnfi324dusers89gsra09ureh0e4")
public class UsersResource {

    private UserRepository userRepository;

    public UsersResource(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/all")
    public List<Users> getAll() {
        return userRepository.findAll();
    }


    @RequestMapping("/getallbyname/{name}")
    public List<Users> getPerson(@PathVariable String name) {
        return userRepository.getByName(name);
    }

    /*@RequestMapping("/getallteamname")
    public List<Users> getByGenre(@RequestParam String teamName) {
        return userRepository.getByTeamName(teamName);
    }*/

}
