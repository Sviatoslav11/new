package com.sv.springmongo.resource;

import com.sv.springmongo.document.Movies;
import com.sv.springmongo.document.Serials;
import com.sv.springmongo.repository.SerialRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/werdfhieusfhoiufqofui23478hnserialsbhwieof879349ijwenfkjafjhg")
public class SerialsResources {

    private SerialRepository serialRepository;

    public SerialsResources(SerialRepository serialRepository) {
        this.serialRepository = serialRepository;
    }

    @GetMapping("/all")
    public List<Serials> getAllMovies() {
        return serialRepository.findAll();
    }

    @RequestMapping("/getbytitle/{title}")
    public List<Serials> getMoviesByTitle(@PathVariable String title) {
        return serialRepository.getByTitle(title);
    }


    @RequestMapping("/getbytype/{type}")
    public List<Serials> getMoviesByTypes(@PathVariable String type) {
        return serialRepository.getByType(type);
    }

    @RequestMapping("/getbygenre/{genre}")
    public List<Serials> getMoviesByGenre(@PathVariable String genre) {
        return serialRepository.getByGenre(genre);
    }
}
