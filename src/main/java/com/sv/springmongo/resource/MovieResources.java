package com.sv.springmongo.resource;

import com.sv.springmongo.document.Movies;
import com.sv.springmongo.repository.MovieRepository;
import com.sv.springmongo.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bdouihcvhbfvksfuoifa124324moviesgvefuwv743t413278r1230gf8hdhjbfj")
public class MovieResources {

    private MovieRepository movieRepository;

    public MovieResources(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @GetMapping("/all")
    public List<Movies> getAllMovies() {
        return movieRepository.findAll();
    }

    @RequestMapping("/getbytitle/{title}")
    public List<Movies> getMoviesByTitle(@PathVariable String title) {
        return movieRepository.getByTitle(title);
    }


    @RequestMapping("/getbytype/{type}")
    public List<Movies> getMoviesByTypes(@PathVariable String type) {
        return movieRepository.getByType(type);
    }
    /*@RequestMapping("/getallteamname")
    public List<Movies> getByGenre(@RequestParam String teamName) {
        return movieRepository.getByTeamName(teamName);
    }*/

    @RequestMapping("/getbygenre/{genre}")
    public List<Movies> getMoviesByGenre(@PathVariable String genre) {
        return movieRepository.getByGenre(genre);
    }

}
