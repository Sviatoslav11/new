package com.sv.springmongo.repository;

import com.sv.springmongo.document.Movies;
import com.sv.springmongo.document.Serials;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SerialRepository extends MongoRepository<Serials, Integer> {

    List<Serials> getByTitle(String Name);

    List<Serials> getByType(String Type);

    List<Serials> getByGenre(String Genre);

}
