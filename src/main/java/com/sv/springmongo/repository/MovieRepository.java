package com.sv.springmongo.repository;


import com.sv.springmongo.document.Movies;
import com.sv.springmongo.document.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MovieRepository extends MongoRepository<Movies, Integer> {

    List<Movies> getByTitle(String title);

    List<Movies> getByType(String Type);

    List<Movies> getByGenre(String Genre);
}
