package com.sv.springmongo.repository;


import com.sv.springmongo.document.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<Users, Integer> {

    List<Users> getByName(String Title);

  //  List<Users> getByType(String Type);
   // List<Users> getByTeamName(String teamName);
}
