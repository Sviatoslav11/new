package com.sv.springmongo.config;

import com.sv.springmongo.document.Movies;
import com.sv.springmongo.document.Serials;
import com.sv.springmongo.document.Users;
import com.sv.springmongo.repository.MovieRepository;
import com.sv.springmongo.repository.SerialRepository;
import com.sv.springmongo.repository.UserRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = MovieRepository.class)
@Configuration
public class MongoDBConfig {


    @Bean
    CommandLineRunner commandLineRunner(MovieRepository movieRepository) {
        return strings -> {
            movieRepository.save(new Movies(1, "ROM", "https://firebasestorage.googleapis.com/v0/b/startupmovie-bef23.appspot.com/o/1%2B1.jpg?alt=media&token=a0fe9d36-e201-4924-8076-b107aa44baf0", "comedy","https://www.youtube.com/watch?v=z572lqKFOts&ab_channel=KINOKOS", "wqfewf", "rr31", "efwf" ,"sqwfg"));
            movieRepository.save(new Movies(2, "ROM", "https://firebasestorage.googleapis.com/v0/b/startupmovie-bef23.appspot.com/o/1%2B1.jpg?alt=media&token=a0fe9d36-e201-4924-8076-b107aa44baf0", "comedy", "https://www.youtube.com/watch?v=z572lqKFOts&ab_channel=KINOKOS", "rr31", "23", "23r", "WF"));
            //userRepository.save(new Users(10, "Sam", "Operations", 2000L,"https://firebasestorage.googleapis.com/v0/b/startupmovie-bef23.appspot.com/o/1%2B1.jpg?alt=media&token=a0fe9d36-e201-4924-8076-b107aa44baf0"));

        };
    }
}



/*
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@Configuration
public class MongoDBConfig {


    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository) {
        return strings -> {
            userRepository.save(new Users(1, "Peter", "Development", 3000L,"https://firebasestorage.googleapis.com/v0/b/startupmovie-bef23.appspot.com/o/1%2B1.jpg?alt=media&token=a0fe9d36-e201-4924-8076-b107aa44baf0"));
            userRepository.save(new Users(2, "Sam", "Operations", 2000L,"https://firebasestorage.googleapis.com/v0/b/startupmovie-bef23.appspot.com/o/1%2B1.jpg?alt=media&token=a0fe9d36-e201-4924-8076-b107aa44baf0"));
            userRepository.save(new Users(10, "Sam", "Operations", 2000L,"https://firebasestorage.googleapis.com/v0/b/startupmovie-bef23.appspot.com/o/1%2B1.jpg?alt=media&token=a0fe9d36-e201-4924-8076-b107aa44baf0"));

        };
    }
}
*/
