package com.sv.springmongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
@Document
public class Serials {

    @Id
    String id;
    String title;
    String image;
    String genre;
    String description;
    String year;
    String casts;
    List<SeasonBean> seasonBean;
    String type;

    public Serials(String id, String title, String image, String genre, String description, String year, String casts, List<SeasonBean> seasonBean, String type) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.genre = genre;
        this.description = description;
        this.year = year;
        this.casts = casts;
        this.seasonBean = seasonBean;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCasts() {
        return casts;
    }

    public void setCasts(String casts) {
        this.casts = casts;
    }

    public List<SeasonBean> getSeasonBean() {
        return seasonBean;
    }

    public void setSeasonBean(List<SeasonBean> seasonBean) {
        this.seasonBean = seasonBean;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}


class SeasonBean {
    String name;
    String image;
    List<String> videoUrls;

    public SeasonBean(String name, String image, List<String> videoUrls) {
        this.name = name;
        this.image = image;
        this.videoUrls = videoUrls;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }
}